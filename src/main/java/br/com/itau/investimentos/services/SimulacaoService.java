package br.com.itau.investimentos.services;


import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Simulacao;
import br.com.itau.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {
    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public Simulacao salvarSimulacao(Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacao;
        simulacao.setMontante(simulacao.getValorAplicado()*Math.pow((simulacao.getInvestimento().getRendimentoAoMes()/100), simulacao.getQuantidadeMeses()));
        simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }

    public Iterable<Simulacao> buscarTodos(){
        Iterable<Simulacao> simulacao = simulacaoRepository.findAll();
        return simulacao;
    }


}
