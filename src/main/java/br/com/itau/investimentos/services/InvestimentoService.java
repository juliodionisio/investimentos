package br.com.itau.investimentos.services;

import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.dtos.InvestimentoDTO;
import br.com.itau.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodos(){
        Iterable<Investimento> investimento = investimentoRepository.findAll();
        return investimento;
    }

    public Investimento buscarPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if(optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        } throw new RuntimeException("O investimento informado não foi encontrado");

    }
}
