package br.com.itau.investimentos.models;


import br.com.itau.investimentos.models.dtos.InvestimentoDTO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idInvestimento;

    private String nome;
    private double rendimentoAoMes;

    /*@OneToMany()
    private List<Simulacao> simulacoes;*/

    public Investimento() {
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public InvestimentoDTO converterParaInvestimentoDTO(){
        InvestimentoDTO investimentoDTO = new InvestimentoDTO();
        investimentoDTO.setNome(this.nome);
        investimentoDTO.setRendimentoAoMes(this.rendimentoAoMes);
        return investimentoDTO;
    }

    /*public List<Simulacao> getSimulacoes() {
        return simulacoes;
    }

    public void setSimulacoes(List<Simulacao> simulacoes) {
        this.simulacoes = simulacoes;
    }*/
}
