package br.com.itau.investimentos.models.dtos;

import br.com.itau.investimentos.models.Investimento;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.Column;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class InvestimentoDTO {

    @Size(min = 5, max = 254, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nome;

    @NotNull(message = "Rendimento não pode ser nullo")
    @NotBlank(message = "Rendimento não pode ser vazio")
    @Column(scale = 2)
    private double rendimentoAoMes;

    public InvestimentoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento converterParaInvestimento(){
        Investimento investimento = new Investimento();
        investimento.setNome(this.nome);
        investimento.setRendimentoAoMes(this.rendimentoAoMes);
        return investimento;
    }
}
