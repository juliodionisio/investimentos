package br.com.itau.investimentos.models.dtos;

import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Simulacao;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

public class SimulacaoDTO {

    @Size(min = 5, max = 254, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nomeInteressado;

    @Email(message = "O formato do email é invalido")
    @NotNull(message = "E-mail não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    private String email;

    @DecimalMax("2")
    @NotNull(message = "Valor aplicado não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    @Column(scale = 2)
    private double valorAplicado;

    @NotNull(message = "Quantidade de meses não pode ser nullo")
    @NotBlank(message = "Não pode ser só espaço")
    private int quantidadeMeses;

    @OneToMany
    @NotNull(message = "É necessário informar um id de investimento")
    @NotBlank(message = "Não pode ser só espaço")
    private int idInvestimento;

    private double rendimentoAoMes;

    @DecimalMax("2")
    @Column(scale = 2) 
    private double montante;

    public SimulacaoDTO() {
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public Simulacao converterParaSimulacao(Investimento investimento){
        Simulacao simulacao = new Simulacao();
        simulacao.setNomeInteressado(this.nomeInteressado);
        simulacao.setEmail(this.email);
        simulacao.setValorAplicado(this.valorAplicado);
        simulacao.setQuantidadeMeses(this.quantidadeMeses);
        simulacao.setInvestimento(investimento);
        return simulacao;
    }
}
