package br.com.itau.investimentos.models;


import br.com.itau.investimentos.models.dtos.SimulacaoDTO;
import br.com.itau.investimentos.services.SimulacaoService;

import javax.persistence.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSimulacao;

    private String nomeInteressado;


    private String email;

    private double valorAplicado;

    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;

    private double montante;

    public Simulacao() {
    }

    public int getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(int idSimulacao) {
        this.idSimulacao = idSimulacao;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento ) {
        this.investimento = investimento;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public SimulacaoDTO converterParaSimulacaoDto(){
        SimulacaoDTO simulacaoDto = new SimulacaoDTO();
        simulacaoDto.setNomeInteressado(this.nomeInteressado);
        simulacaoDto.setEmail(this.email);
        simulacaoDto.setValorAplicado(this.valorAplicado);
        simulacaoDto.setQuantidadeMeses(this.quantidadeMeses);
        simulacaoDto.setIdInvestimento(this.investimento.getIdInvestimento());
        simulacaoDto.setRendimentoAoMes(this.investimento.getRendimentoAoMes());
        simulacaoDto.setMontante(this.getMontante());
        return simulacaoDto;
    }
}
