package br.com.itau.investimentos.repositories;


import br.com.itau.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Long> {
}
