package br.com.itau.investimentos.controllers;

import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Simulacao;
import br.com.itau.investimentos.models.dtos.InvestimentoDTO;
import br.com.itau.investimentos.models.dtos.SimulacaoDTO;
import br.com.itau.investimentos.services.InvestimentoService;
import br.com.itau.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    public ResponseEntity<InvestimentoDTO> registrarInvestimento(@RequestBody  InvestimentoDTO investimentoDto){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento
                (investimentoDto.converterParaInvestimento());
        return ResponseEntity.status(201).body(investimentoObjeto.converterParaInvestimentoDTO());
    }

    @GetMapping
    public List<InvestimentoDTO> exibirTodos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodos();
        List<InvestimentoDTO> investimentosDto = new ArrayList<InvestimentoDTO>();
        for(Investimento investimento: investimentos){

                investimentosDto.add(investimento.converterParaInvestimentoDTO());

        }
        return investimentosDto;
    }

    @PostMapping("/{id}/simulacao")
    public ResponseEntity<SimulacaoDTO> simularInvestimento(@PathVariable int id,
                                                            @RequestBody SimulacaoDTO simulacaoDTO){
        id++;
        simulacaoDTO.setIdInvestimento(id);
        Simulacao simulacaoObjeto = simulacaoService.salvarSimulacao
                (simulacaoDTO.converterParaSimulacao(investimentoService.buscarPorId(id)));
        return ResponseEntity.status(201).body(simulacaoObjeto.converterParaSimulacaoDto());
    }


}
