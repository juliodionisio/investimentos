package br.com.itau.investimentos.controllers;


import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Simulacao;
import br.com.itau.investimentos.models.dtos.InvestimentoDTO;
import br.com.itau.investimentos.models.dtos.SimulacaoDTO;
import br.com.itau.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public List<SimulacaoDTO> exibirTodos(){
        Iterable<Simulacao> simulacoes = simulacaoService.buscarTodos();
        List<SimulacaoDTO> simulacoesDto = new ArrayList<SimulacaoDTO>();
        for(Simulacao simulacao: simulacoes){

            simulacoesDto.add(simulacao.converterParaSimulacaoDto());

        }
        return simulacoesDto;
    }




}
